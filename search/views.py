# coding: utf8
# App de recherche pour amap
# Augustin lemesle, Gabriel Détraz, Goulven Kermarec
# Gplv2
from django.shortcuts import render
from django.shortcuts import render_to_response, get_object_or_404
from django.template.context_processors import csrf
from django.template import Context, RequestContext, loader
from django.contrib.auth.decorators import login_required

from django.db.models import Q
from users.models import User

from search.models import SearchForm, SearchFormPlus
from panier.models import Commande, Panier, Credit, Livraison

from amap.settings import SEARCH_RESULT

def form(ctx, template, request):
    c = ctx
    c.update(csrf(request))
    return render(request, template, c)

def search_result(search, type, request):
    date_deb = None
    date_fin = None
    date_livraison = None 
    states=[]
    co=[]
    aff=[]
    if(type):
        aff = search.cleaned_data['affichage']
        states = search.cleaned_data['filtre']
        date_deb = search.cleaned_data['date_deb']
        date_fin = search.cleaned_data['date_fin']
        date_livraison = search.cleaned_data['date_livraison']
    date_query = Q()
    if aff==[]:
        aff = ['0','1','2','3']
    if date_deb != None:
        date_query = date_query & Q(date__gte=date_deb)
    if date_fin != None:
        date_query = date_query & Q(date__lte=date_fin)
    if date_livraison != None:
        date_query = date_query & Q(date=date_livraison.date) 
    search = search.cleaned_data['search_field']
    query = Q() 
    for s in states:
        query = query | Q(state = s)
    
    users = None
    commande = None
    panier = None
    credit = None

    for i in aff:
        if i == '0':
            users = User.objects.filter((Q(pseudo__icontains = search) | Q(name__icontains = search) | Q(surname__icontains = search)) & query)[:SEARCH_RESULT]
            query = Q(user__pseudo__icontains = search) | Q(user__name__icontains = search) | Q(user__surname__icontains = search)
            if not request.user.has_perms(('bureau',)):
                users = [request.user]
        if i == '1':
            panier = Panier.objects.filter(nom__icontains = search)[:SEARCH_RESULT]
        if i == '2':   
            commande = Commande.objects.filter(Q(date__in=Livraison.objects.filter(date_query)) & query)[:SEARCH_RESULT]
            if not request.user.has_perms(('bureau',)):
                commande = commande.filter(user=request.user)[:SEARCH_RESULT]
        if i == '3':    
            credit = Credit.objects.filter(query)[:SEARCH_RESULT]
            if not request.user.has_perms(('bureau',)):
                credit = credit.filter(user=request.user)
    return {'users_list': users, 'article_list' : panier, 'paniers_list' : commande, 'credit_list' : credit , 'max_result' : SEARCH_RESULT}

@login_required
def search(request):
    search = SearchForm(request.POST or None)
    if search.is_valid():
        return form(search_result(search, False, request), 'search/index.html',request)
    return form({'searchform' : search}, 'search/search.html', request)

@login_required
def searchp(request):
    search = SearchFormPlus(request.POST or None)
    if search.is_valid():
        return form(search_result(search, True, request), 'search/index.html',request)
    return form({'searchform' : search}, 'search/search.html', request)
