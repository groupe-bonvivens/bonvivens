from users.models import User
from datetime import datetime
import pytz

def recap():
	trigger=datetime(2017,7,15,0,0,0,0,pytz.UTC)
	print("Recherche avant la date: {}".format(trigger))
	vieux_riches=[]
	vieux_pauvres=[]
	for plop in User.objects.all():

		last=plop.commande_set.last()
		if(last!=None and last.date.date<trigger):
			if(plop.solde()<0):
				vieux_riches.append((plop.name,last.date.date,plop.solde(),plop.email))
			elif(plop.solde()>0):
				vieux_riches.append((plop.name,last.date.date,plop.solde(),plop.email))

	dette=0
	gain=0
	for i in vieux_riches:
		print("|{:<20}|{:<15}|{:<10}|{:^40}|".format(i[0],str(i[1].year)+"-"+str(i[1].month)+"-"+str(i[1].day),i[2],i[3]))
		dette+=i[2]
	print("Somme totale due: {}".format(dette))
	print("\n\n\n")
	for i in vieux_riches:
		print("|{:<20}|{:<15}|{:<10}|{:^40}|".format(i[0],str(i[1].year)+"-"+str(i[1].month)+"-"+str(i[1].day),i[2],i[3]))
		gain+=i[2]
	print("Somme totale gagnée: {}".format(gain))
	print("FIN")

def negatifs():
	"""
	Récupere les utilisateurs en négatif et prépare un mail
	"""
	negatifs=[plop for plop in User.objects.all() if plop.solde()<0]
	for user in negatifs:

		solde=user.solde()
		nom=user.name
		maddr=user.email

		print("Écriture du mail pour {}".format(nom))

		lignes=[]
		lignes.append("Bonjour {},".format(nom))
		lignes.append('\n')
		lignes.append('Tu reçois ce mail car tu est adhérent de NormalSoup\' et que ton solde est actuellemet de {}€. Nous te prions de régulariser cette situation au plus vite.'.format(solde))
		lignes.append('\n')
		lignes.append('Pour cela tu peux envoyer un mail à amap-bureau@lists.crans.org en laissant apparaitre [REGULARISATION] dans l\'objet. Tu peux également venir le jeudi aupavillon des jardins entre 17h30 et 19h pour regler en espèce ou par chèque.')
		lignes.append('\n')
		lignes.append('Légumineusement.')
		lignes.append('\n')
		lignes.append('L\'équipe de NormalSoup\'.')

		mail=open('scripts/mails_negatif/'+maddr+'.txt','w')
		for ligne in lignes:
			mail.write(ligne)
		mail.close()

	print("Fin de l'écriture de mails")




