from django.shortcuts import render
from django.shortcuts import render_to_response, get_object_or_404
from django.template.context_processors import csrf
from django.template import Context, RequestContext, loader

def form(ctx, template, request):
    c = ctx
    c.update(csrf(request))
    return render(request, template, c)


def index(request):
    return form({}, 'amap/index.html', request)
