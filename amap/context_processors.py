from .settings import SITE_NAME
from users.models import Right

def context_user(request):
    user = request.user
    is_bureau = user.has_perms(('bureau',))
    if user.is_authenticated():
        list_droits = Right.objects.filter(user=user)
    else:
        list_droits = None 
    return {
        'request_user': user,
        'is_bureau': is_bureau,
        'list_droits': list_droits,
        'site_name': SITE_NAME,
    }
