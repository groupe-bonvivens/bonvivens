# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('panier', '0006_livraison_date_modif'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commande',
            name='quantite',
            field=models.PositiveIntegerField(validators=[django.core.validators.MinValueValidator(1)]),
        ),
    ]
