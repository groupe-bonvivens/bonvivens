# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Commande',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('quantite', models.IntegerField()),
                ('prix', models.DecimalField(max_digits=5, decimal_places=2)),
                ('livre', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Credit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('montant', models.DecimalField(max_digits=5, decimal_places=2)),
                ('validite', models.BooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='Livraison',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('date', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Paiement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('moyen', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Panier',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('prix', models.DecimalField(max_digits=5, decimal_places=2)),
                ('quantite', models.IntegerField()),
                ('nom', models.CharField(max_length=255)),
            ],
        ),
        migrations.AddField(
            model_name='credit',
            name='moyen',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='panier.Paiement'),
        ),
        migrations.AddField(
            model_name='credit',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='commande',
            name='date',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='panier.Livraison'),
        ),
        migrations.AddField(
            model_name='commande',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL),
        ),
    ]
