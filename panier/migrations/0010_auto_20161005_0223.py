# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('panier', '0009_auto_20161005_0219'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commande',
            name='coefficient_panier',
            field=models.DecimalField(decimal_places=1, null=True, blank=True, max_digits=2),
        ),
        migrations.AlterField(
            model_name='panier',
            name='coefficient_panier',
            field=models.DecimalField(decimal_places=1, null=True, blank=True, max_digits=2),
        ),
    ]
