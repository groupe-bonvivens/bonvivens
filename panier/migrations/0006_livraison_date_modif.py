# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('panier', '0005_auto_20160905_0207'),
    ]

    operations = [
        migrations.AddField(
            model_name='livraison',
            name='date_modif',
            field=models.DateTimeField(default=datetime.datetime(2016, 9, 5, 0, 51, 14, 377006, tzinfo=utc), help_text='%d/%m/%y %H:%M:%S'),
            preserve_default=False,
        ),
    ]
