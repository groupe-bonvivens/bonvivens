# coding: utf8
from django import forms
from django.forms import ModelForm, Form
from django import forms
from django.core.validators import MinValueValidator
from .models import Panier, Paiement, Commande, Credit, Livraison
from dateutil.relativedelta import relativedelta
from django.utils import timezone

class NewCommandeForm(ModelForm):
    def __init__(self,*args,**kwargs):
        is_bureau=kwargs.pop('is_bureau')
        super (NewCommandeForm,self ).__init__(*args,**kwargs) # populates the post
        if is_bureau:
            self.fields['date'].queryset = Livraison.objects.filter(date__gt=(timezone.now()+relativedelta(weeks=-2)))
        else:
            self.fields['date'].queryset = Livraison.objects.filter(date__gt=timezone.now())

    class Meta:
        model = Commande
        fields = ['quantite','date']

class BaseEditCommandeForm(ModelForm):
    class Meta:
        model = Commande
        fields = ['quantite','date']

class FullEditCommandeForm(BaseEditCommandeForm):
    class Meta(BaseEditCommandeForm.Meta):
        fields = '__all__'

class SelectArticleForm(ModelForm):
    article = forms.ModelChoiceField(queryset=Panier.objects.all(), label="Articles en vente actuellement")

    class Meta:
        exclude = ['nom','prix','coefficient_panier']
        model = Panier


class LivraisonForm(ModelForm):
    class Meta:
        model = Livraison
        fields = ['date','date_modif']

    def __init__(self, *args, **kwargs):
        super(LivraisonForm, self).__init__(*args, **kwargs)
        self.fields['date'].label = 'Date de livraison'
        self.fields['date_modif'].label = 'Date limite de modification'

class SelectLivraisonForm(ModelForm):
    date = forms.ModelMultipleChoiceField(queryset=Livraison.objects.all(), label="Date de livraison",  widget=forms.CheckboxSelectMultiple)

    class Meta:
        exclude = ['date', 'date_modif']
        model = Livraison

class SelectOneLivraisonForm(ModelForm):
    date = forms.ModelChoiceField(queryset=Livraison.objects.all().order_by('date').reverse(), label="Date de livraison")

    class Meta:
        exclude = ['date', 'date_modif']
        model = Livraison

class PaiementForm(ModelForm):
    class Meta:
        model = Paiement
        fields = ['moyen']

    def __init__(self, *args, **kwargs):
        super(PaiementForm, self).__init__(*args, **kwargs)
        self.fields['moyen'].label = 'Moyen de paiement à ajouter'

class DelPaiementForm(ModelForm):
    paiements = forms.ModelMultipleChoiceField(queryset=Paiement.objects.all(), label="Moyens de paiement actuels",  widget=forms.CheckboxSelectMultiple)

    class Meta:
        exclude = ['moyen']
        model = Paiement

class PanierForm(ModelForm):
    class Meta:
        model = Panier
        fields = ['nom','prix','coefficient_panier']

class DelPanierForm(ModelForm):
    paniers = forms.ModelMultipleChoiceField(queryset=Panier.objects.all(), label="Articles en vente actuellement",  widget=forms.CheckboxSelectMultiple)

    class Meta:
        exclude = ['nom','prix','coefficient_panier']
        model = Panier

class CreditForm(ModelForm):
    class Meta:
        model = Credit
        fields = ['montant','moyen','validite']

class CreditFullForm(CreditForm):
    class Meta(CreditForm.Meta):
        fields = '__all__'

class CreditNoteForm(forms.Form):
        """A special form to get credential and amount to connect and make a credit using the API of NoteKfet2015 server
        """
        login = forms.CharField(
            label="Pseudo note"
        )
        password = forms.CharField(
            label="Password",
            widget=forms.PasswordInput
        )
        montant = forms.FloatField(
            label="Montant"
        )

