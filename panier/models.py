# coding: utf8
from django.db import models

from django.forms import ModelForm, Form
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from django.core.validators import MaxValueValidator, MinValueValidator

class Panier(models.Model):
    prix = models.DecimalField(max_digits=5, decimal_places=2)
    nom = models.CharField(max_length=255)
    coefficient_panier = models.DecimalField(max_digits=3, decimal_places=2, null=True)

    def __str__(self):
        return self.nom

class Commande(models.Model):
    user = models.ForeignKey('users.User', on_delete=models.PROTECT)
    nom = models.CharField(max_length=255)
    coefficient_panier = models.DecimalField(max_digits=3, decimal_places=2, null=True)
    quantite = models.PositiveIntegerField(validators=[MinValueValidator(1)])
    prix_unitaire = models.DecimalField(max_digits=5, decimal_places=2)
    livre = models.BooleanField(default=False)
    date = models.ForeignKey('Livraison', on_delete=models.PROTECT)

    def prix(self):
        return self.prix_unitaire*self.quantite

class Credit(models.Model):
    user = models.ForeignKey('users.User', on_delete=models.PROTECT)
    montant = models.DecimalField(max_digits=5, decimal_places=2)
    moyen = models.ForeignKey('Paiement', on_delete=models.PROTECT)
    validite = models.BooleanField(default=True)

    def __str__(self):
        return str(self.montant) + "€ pour " +  str(self.user)

class Paiement(models.Model):
    moyen = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.moyen

class Livraison(models.Model):
    date = models.DateTimeField(help_text='%d/%m/%y %H:%M:%S')
    date_modif = models.DateTimeField(help_text='%d/%m/%y %H:%M:%S')

    def __str__(self):
        return str(self.date)
