from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from reversion.admin import VersionAdmin

from .models import Panier, Commande, Credit, Paiement, Livraison

class PanierAdmin(VersionAdmin):
    list_display = ('prix','coefficient_panier','nom')

class CommandeAdmin(VersionAdmin):
    list_display = ('user', 'quantite', 'coefficient_panier', 'prix_unitaire', 'nom', 'livre', 'date')

class CreditAdmin(VersionAdmin):
    list_display = ('user','montant','moyen','validite')

class PaiementAdmin(VersionAdmin):
    list_display = ('moyen',)

class LivraisonAdmin(VersionAdmin):
    list_display = ('date',)

admin.site.register(Panier, PanierAdmin)
admin.site.register(Commande, CommandeAdmin)
admin.site.register(Credit, CreditAdmin)
admin.site.register(Paiement, PaiementAdmin)
admin.site.register(Livraison, LivraisonAdmin)
