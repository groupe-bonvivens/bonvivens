from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^new_commande/(?P<userid>[0-9]+)$', views.new_commande, name='new-commande'),
    url(r'^edit_commande/(?P<commandeid>[0-9]+)$', views.edit_commande, name='edit-commande'),
    url(r'^del_commande/(?P<commandeid>[0-9]+)$', views.del_commande, name='del-commande'),
    url(r'^add_paiement/$', views.add_paiement, name='add-paiement'),
    url(r'^edit_paiement/(?P<paiementid>[0-9]+)$', views.edit_paiement, name='edit-paiement'),
    url(r'^del_paiement/$', views.del_paiement, name='del-paiement'),
    url(r'^index_paiement/$', views.index_paiement, name='index-paiement'),
    url(r'^add_livraison/$', views.add_livraison, name='add-livraison'),
    url(r'^edit_livraison/(?P<livraisonid>[0-9]+)$', views.edit_livraison, name='edit-livraison'),
    url(r'^del_livraison/$', views.del_livraison, name='del-livraison'),
    url(r'^index_livraison/$', views.index_livraison, name='index-livraison'),
    url(r'^add_credit/(?P<userid>[0-9]+)$', views.add_credit, name='add-credit'),
    url(r'^add_credit_note/(?P<userid>[0-9]+)$', views.add_credit_note, name='add-credit-note'),
    url(r'^edit_credit/(?P<creditid>[0-9]+)$', views.edit_credit, name='edit-credit'),
    url(r'^del_credit/(?P<creditid>[0-9]+)$', views.del_credit, name='del-credit'),
    url(r'^add_article/$', views.add_article, name='add-article'),
    url(r'^edit_article/(?P<articleid>[0-9]+)$', views.edit_article, name='edit-article'),
    url(r'^del_article/$', views.del_article, name='del-article'),
    url(r'^add_sac/(?P<userid>[0-9]+)$', views.add_sac, name='add-sac'),
    url(r'^del_sac/(?P<userid>[0-9]+)$', views.del_sac, name='del-sac'),
    url(r'^plusProcheLivraison/$', views.plusProcheLivraison, name='plusProcheLivraison'),
    url(r'^control/$', views.control, name='control'),
    url(r'^control/(?P<livraisonid>[0-9]+)$', views.control, name='control'),
    url(r'^index_article/$', views.index_article, name='index-article'),
    url(r'^history/(?P<object>paiement)/(?P<id>[0-9]+)$', views.history, name='history'),
    url(r'^history/(?P<object>livraison)/(?P<id>[0-9]+)$', views.history, name='history'),
    url(r'^history/(?P<object>credit)/(?P<id>[0-9]+)$', views.history, name='history'),
    url(r'^history/(?P<object>article)/(?P<id>[0-9]+)$', views.history, name='history'),
    url(r'^history/(?P<object>commande)/(?P<id>[0-9]+)$', views.history, name='history'),
    url(r'^$', views.index, name='index'),
]


